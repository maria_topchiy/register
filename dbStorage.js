const sql = require('yesql').pg
const config = require('./config');
const { db: { user, host, name, password, port} } = config;
const { Client } = require('pg');

class DbStorage 
{
    connected = false;

    constructor() {
        this.client = new Client({
            user: user,
            host: host,
            database: name,
            password: password,
            port: port,
        });  
    }

    connect()
    { 
        if (!this.connected) {
            return this.client.connect().catch(e => {
                return Promise.resolve();
            });
        }
        return Promise.resolve();
    }

    readItems(query, values)
    {
        if (!this.connected) {
            return this.connect()
                .then(() => {
                    console.log('Connected successfully');
                    this.connected = true;
                    return this.client.query(sql(query)(values))
                })
                .then((res) => {
                    return res.rows;
                });
        }
        return this.client.query(sql(query)(values)).then(res => {
            return res.rows;
        });
    }
}

const dbStorage = new DbStorage();

module.exports = dbStorage;