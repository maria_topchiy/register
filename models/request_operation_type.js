class RequestOperationType
{
    constructor(id, operationType) 
    {
        this.id = id;
        this.operationType = operationType;
    }
};
 
module.exports = RequestOperationType;