class Organisation
{
    constructor(name, region, city, street, numberOfBuilding) 
    {
        this.name = name;
        this.region = region;
        this.city = city;
        this.street = street;
        this.numberOfBuilding = numberOfBuilding;
    }
};
 
module.exports = Organisation;