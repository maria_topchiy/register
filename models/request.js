class Request
{
    constructor(id, date, comment, operationType, status, certificate, expert, commission, commissionDecision, dismissalOrderDate, dismissalOrderNumber, author) 
    {
        this.id = id;
        this.date = date;
        this.comment = comment;
        this.operationType = operationType;
        this.status = status;
        this.certificate = certificate;
        this.expert = expert;
        this.commission = commission;
        this.commissionDecision = commissionDecision;
        this.dismissalOrderDate = dismissalOrderDate;
        this.dismissalOrderNumber = dismissalOrderNumber;
        this.author = author;
    }
};
 
module.exports = Request;