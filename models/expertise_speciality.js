class ExpertiseSpeciality
{
    constructor(id, number, name) 
    {
        this.id = id;
        this.number = number;
        this.name = name;
    }
};
 
module.exports = ExpertiseSpeciality;