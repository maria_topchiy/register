class Commission
{
    constructor(name, authority) 
    {
        this.name = name;
        this.authority = authority;
    }
};
 
module.exports = Commission;