class Certificate
{
    constructor(id, documentNumber, validityEndDate, expertiseSpecialities, expertiseType, commission, commissionDecision) 
    {
        this.id = id;
        this.documentNumber = documentNumber;
        this.validityEndDate = validityEndDate;
        this.expertiseSpecialities = expertiseSpecialities;
        this.expertiseType = expertiseType;
        this.commission = commission;
        this.commissionDecision = commissionDecision;
    }
};
 
module.exports = Certificate;