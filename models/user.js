class AuthorizedUser
{
    constructor(id, role, firstName, lastName, patronymic, birthdate, passport, taxpayerRegistartionNumber, email, password, isActive) 
    {
        this.id = id;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthdate = birthdate;
        this.passport = passport;
        this.taxpayerRegistartionNumber = taxpayerRegistartionNumber;
        this.email = email;
        this.password = password;
        this.isActive = isActive;
    }
};
 
module.exports = AuthorizedUser;