class CommissionDecision
{
    constructor(number, date, type) 
    {
        this.number = number;
        this.date = date;
        this.type = type;
    }
};
 
module.exports = CommissionDecision;