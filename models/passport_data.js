class PassportData
{
    constructor(number, series, dateOfIssue, authorityCode) 
    {
        this.number = number;
        this.series = series;
        this.dateOfIssue = dateOfIssue;
        this.authorityCode = authorityCode;
    }
};
 
module.exports = PassportData;