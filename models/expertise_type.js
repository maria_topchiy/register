class ExpertiseType
{
    constructor(id, number, name, expertiseSpecialities) 
    {
        this.id = id;
        this.number = number;
        this.name = name;
        this.expertiseSpecialities = expertiseSpecialities;
    }
};
 
module.exports = ExpertiseType;