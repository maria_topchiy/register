class Expert
{
    constructor(id, firstName, lastName, patronymic, contacts, organisation, authority, isFromGovernment) 
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.contacts = contacts;
        this.organisation = organisation;
        this.authority = authority;
        this.isFromGovernment = isFromGovernment;
    }
};
 
module.exports = Expert;