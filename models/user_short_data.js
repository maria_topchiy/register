class AuthorizedUserShortData
{
    constructor(id, role, firstName, lastName, patronymic, email) 
    {
        this.id = id;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.email = email;
    }
};
 
module.exports = AuthorizedUserShortData;