class ExpertiseClass
{
    constructor(id, classNumber, name, expertiseTypes) 
    {
        this.id = id;
        this.classNumber = classNumber;
        this.name = name;
        this.expertiseTypes = expertiseTypes;
    }
};
 
module.exports = ExpertiseClass;