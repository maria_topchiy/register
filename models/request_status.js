class RequestStatus
{
    constructor(id, status) 
    {
        this.id = id;
        this.status = status;
    }
};
 
module.exports = RequestStatus;