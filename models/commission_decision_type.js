class CommissionDecisionType
{
    constructor(id, type) 
    {
        this.id = id;
        this.type = type;
    }
};
 
module.exports = CommissionDecisionType;