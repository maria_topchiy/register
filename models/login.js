class Login
{
    constructor(jwt_token, role, id) 
    {
        this.jwt_token = jwt_token;
        this.role = role;
        this.id = id;
    }
};
 
module.exports = Login;