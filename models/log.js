class Log
{
    constructor(id, author, affectedUser, date, typeOfChange, request) 
    {
        this.id = id;
        this.author = author;
        this.affectedUser = affectedUser;
        this.date = date;
        this.typeOfChange = typeOfChange;
        this.request = request;
    }
};
 
module.exports = Log;