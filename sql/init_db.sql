-- Table: public.Authorities

-- DROP TABLE public."Authorities";

CREATE TABLE public."Authorities"
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "StateAgencies_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."Authorities"
    OWNER to postgres;



-- Table: public.AuthorizedUsersRoles

-- DROP TABLE public."AuthorizedUsersRoles";

CREATE TABLE public."AuthorizedUsersRoles"
(
    id serial NOT NULL,
    role character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "AuthorizedUsersRoles_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."AuthorizedUsersRoles"
    OWNER to postgres;



-- Table: public.CommissionsDecisionsTypes

-- DROP TABLE public."CommissionsDecisionsTypes";

CREATE TABLE public."CommissionsDecisionsTypes"
(
    id serial NOT NULL,
    type text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "DecisionsTypes_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."CommissionsDecisionsTypes"
    OWNER to postgres;



-- Table: public.PassportAuthorities

-- DROP TABLE public."PassportAuthorities";

CREATE TABLE public."PassportAuthorities"
(
    id serial NOT NULL,
    code character(4) COLLATE pg_catalog."default" NOT NULL,
    name_of_unit text COLLATE pg_catalog."default" NOT NULL,
    name_of_authority text COLLATE pg_catalog."default" NOT NULL,
    address_city text COLLATE pg_catalog."default" NOT NULL,
    address_street text COLLATE pg_catalog."default" NOT NULL,
    address_building integer NOT NULL,
    is_active boolean NOT NULL,
    CONSTRAINT "PassportAuthorities_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."PassportAuthorities"
    OWNER to postgres;
	


-- Table: public.RequestOperationTypes

-- DROP TABLE public."RequestOperationTypes";

CREATE TABLE public."RequestOperationTypes"
(
    id serial NOT NULL,
    operation_type text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "RequestOperationTypes_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."RequestOperationTypes"
    OWNER to postgres;



-- Table: public.RequestStatuses

-- DROP TABLE public."RequestStatuses";

CREATE TABLE public."RequestStatuses"
(
    id serial NOT NULL,
    status text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "RequestStatuses_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."RequestStatuses"
    OWNER to postgres;



-- Table: public.TypesOfChanges

-- DROP TABLE public."TypesOfChanges";

CREATE TABLE public."TypesOfChanges"
(
    id serial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "TypesOfChanges_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."TypesOfChanges"
    OWNER to postgres;



-- Table: public.ExpertiseClasses

-- DROP TABLE public."ExpertiseClasses";

CREATE TABLE public."ExpertiseClasses"
(
    id serial NOT NULL,
    class_number smallint NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "ExpertiseClasses_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."ExpertiseClasses"
    OWNER to postgres;



-- Table: public.ExpertiseTypes

-- DROP TABLE public."ExpertiseTypes";

CREATE TABLE public."ExpertiseTypes"
(
    id serial NOT NULL,
    expertise_number smallint NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    class_id smallint NOT NULL,
    CONSTRAINT "ExpertiseTypes_pkey" PRIMARY KEY (id),
    CONSTRAINT class FOREIGN KEY (class_id)
        REFERENCES public."ExpertiseClasses" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."ExpertiseTypes"
    OWNER to postgres;
-- Index: fki_class

-- DROP INDEX public.fki_class;

CREATE INDEX fki_class
    ON public."ExpertiseTypes" USING btree
    (class_id ASC NULLS LAST)
    TABLESPACE pg_default;



-- Table: public.ExpertSpecialityTypes

-- DROP TABLE public."ExpertSpecialityTypes";

CREATE TABLE public."ExpertSpecialityTypes"
(
    id serial NOT NULL,
    index numeric(4,1) NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    expertise_id smallint NOT NULL,
    CONSTRAINT "ExpertSpecialityTypes_pkey" PRIMARY KEY (id),
    CONSTRAINT expertise FOREIGN KEY (expertise_id)
        REFERENCES public."ExpertiseTypes" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."ExpertSpecialityTypes"
    OWNER to postgres;



-- Table: public.AuthorizedUsers

-- DROP TABLE public."AuthorizedUsers";

CREATE TABLE public."AuthorizedUsers"
(
    id serial NOT NULL,
    first_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    patronymic character varying(50) COLLATE pg_catalog."default",
    role_id integer NOT NULL,
    birthdate date NOT NULL,
    passport_number character(9) COLLATE pg_catalog."default" NOT NULL,
    passport_series character(2) COLLATE pg_catalog."default",
    passport_date_of_issue date NOT NULL,
    taxpayer_registration_number character(10) COLLATE pg_catalog."default",
    login character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password character varying(50) COLLATE pg_catalog."default" NOT NULL,
    passport_authority_id integer NOT NULL,
    CONSTRAINT "AuthorizedUsers_pkey" PRIMARY KEY (id),
    CONSTRAINT passport_authority FOREIGN KEY (passport_authority_id)
        REFERENCES public."PassportAuthorities" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT role FOREIGN KEY (role_id)
        REFERENCES public."AuthorizedUsersRoles" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."AuthorizedUsers"
    OWNER to postgres;
-- Index: fki_passport_authority

-- DROP INDEX public.fki_passport_authority;

CREATE INDEX fki_passport_authority
    ON public."AuthorizedUsers" USING btree
    (passport_authority_id ASC NULLS LAST)
    TABLESPACE pg_default;



-- Table: public.Experts

-- DROP TABLE public."Experts";

CREATE TABLE public."Experts"
(
    id serial NOT NULL,
    first_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    patronymic character varying(50) COLLATE pg_catalog."default",
    organization_city text COLLATE pg_catalog."default" NOT NULL,
    organization_street text COLLATE pg_catalog."default" NOT NULL,
    organization_number_of_building smallint NOT NULL,
    organization text COLLATE pg_catalog."default" NOT NULL,
    email character varying(50) COLLATE pg_catalog."default",
    phone character(12) COLLATE pg_catalog."default" NOT NULL,
    is_from_government boolean NOT NULL,
    authority_id integer NOT NULL,
    CONSTRAINT "Experts_pkey" PRIMARY KEY (id),
    CONSTRAINT authorities FOREIGN KEY (authority_id)
        REFERENCES public."Authorities" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."Experts"
    OWNER to postgres;
-- Index: fki_state_agency

-- DROP INDEX public.fki_state_agency;

CREATE INDEX fki_state_agency
    ON public."Experts" USING btree
    (authority_id ASC NULLS LAST)
    TABLESPACE pg_default;



-- Table: public.Certificates

-- DROP TABLE public."Certificates";

CREATE TABLE public."Certificates"
(
    id serial NOT NULL,
    document_number integer NOT NULL,
    validity_end_date date NOT NULL,
    expert_id integer NOT NULL,
    CONSTRAINT "Certificates_pkey" PRIMARY KEY (id),
    CONSTRAINT expert FOREIGN KEY (expert_id)
        REFERENCES public."Experts" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."Certificates"
    OWNER to postgres;
-- Index: fki_expert

-- DROP INDEX public.fki_expert;

CREATE INDEX fki_expert
    ON public."Certificates" USING btree
    (expert_id ASC NULLS LAST)
    TABLESPACE pg_default;



-- Table: public.CertificatesExpertSpecialityTypes

-- DROP TABLE public."CertificatesExpertSpecialityTypes";

CREATE TABLE public."CertificatesExpertSpecialityTypes"
(
    id serial NOT NULL,
    certificate_id integer NOT NULL,
    speciality_type_id integer NOT NULL,
    CONSTRAINT "CertificatesExpertSpecialityTypes_pkey" PRIMARY KEY (id),
    CONSTRAINT certificate_fkey FOREIGN KEY (certificate_id)
        REFERENCES public."Certificates" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT speciality_type_fkey FOREIGN KEY (speciality_type_id)
        REFERENCES public."ExpertSpecialityTypes" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."CertificatesExpertSpecialityTypes"
    OWNER to postgres;



-- Table: public.Requests

-- DROP TABLE public."Requests";

CREATE TABLE public."Requests"
(
    id serial NOT NULL,
    operation_type_id integer NOT NULL,
    dismissal_order_date date,
    dismissal_order_number integer,
    status_id integer NOT NULL,
    date date NOT NULL,
    comment text COLLATE pg_catalog."default",
    commission_decision_number integer NOT NULL,
    commission_decision_date date NOT NULL,
    commission_decision_type_id integer,
    certificate_id integer NOT NULL,
    commission text COLLATE pg_catalog."default" NOT NULL,
    author_id integer NOT NULL,
    commission_authority_id integer NOT NULL,
    CONSTRAINT "Requests_pkey" PRIMARY KEY (id),
    CONSTRAINT author FOREIGN KEY (author_id)
        REFERENCES public."AuthorizedUsers" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT certificate FOREIGN KEY (certificate_id)
        REFERENCES public."Certificates" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT commission_authority FOREIGN KEY (commission_authority_id)
        REFERENCES public."Authorities" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT commission_decision_type FOREIGN KEY (commission_decision_type_id)
        REFERENCES public."CommissionsDecisionsTypes" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT operation_type FOREIGN KEY (operation_type_id)
        REFERENCES public."RequestOperationTypes" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT status FOREIGN KEY (status_id)
        REFERENCES public."RequestStatuses" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."Requests"
    OWNER to postgres;
-- Index: fki_certificate

-- DROP INDEX public.fki_certificate;

CREATE INDEX fki_certificate
    ON public."Requests" USING btree
    (certificate_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_comission_authority

-- DROP INDEX public.fki_comission_authority;

CREATE INDEX fki_comission_authority
    ON public."Requests" USING btree
    (commission_authority_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_comission_decision_type

-- DROP INDEX public.fki_comission_decision_type;

CREATE INDEX fki_comission_decision_type
    ON public."Requests" USING btree
    (commission_decision_type_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_operation_type

-- DROP INDEX public.fki_operation_type;

CREATE INDEX fki_operation_type
    ON public."Requests" USING btree
    (operation_type_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_status

-- DROP INDEX public.fki_status;

CREATE INDEX fki_status
    ON public."Requests" USING btree
    (status_id ASC NULLS LAST)
    TABLESPACE pg_default;



-- Table: public.Logs

-- DROP TABLE public."Logs";

CREATE TABLE public."Logs"
(
    id serial NOT NULL,
    author_id integer NOT NULL,
    type_of_change_id integer NOT NULL,
    date date NOT NULL,
    affected_user_id integer NOT NULL,
    request_operation_type_id integer,
    request_id integer,
    CONSTRAINT "Logs_pkey" PRIMARY KEY (id),
    CONSTRAINT affected_user FOREIGN KEY (affected_user_id)
        REFERENCES public."AuthorizedUsers" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT author FOREIGN KEY (author_id)
        REFERENCES public."AuthorizedUsers" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT request FOREIGN KEY (request_id)
        REFERENCES public."Requests" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT request_operation_type FOREIGN KEY (request_operation_type_id)
        REFERENCES public."RequestOperationTypes" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT type_of_change FOREIGN KEY (type_of_change_id)
        REFERENCES public."TypesOfChanges" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public."Logs"
    OWNER to postgres;
-- Index: fki_affected_user

-- DROP INDEX public.fki_affected_user;

CREATE INDEX fki_affected_user
    ON public."Logs" USING btree
    (affected_user_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_author

-- DROP INDEX public.fki_author;

CREATE INDEX fki_author
    ON public."Logs" USING btree
    (author_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_request

-- DROP INDEX public.fki_request;

CREATE INDEX fki_request
    ON public."Logs" USING btree
    (request_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_request_operation_type

-- DROP INDEX public.fki_request_operation_type;

CREATE INDEX fki_request_operation_type
    ON public."Logs" USING btree
    (request_operation_type_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_type_of_change

-- DROP INDEX public.fki_type_of_change;

CREATE INDEX fki_type_of_change
    ON public."Logs" USING btree
    (type_of_change_id ASC NULLS LAST)
    TABLESPACE pg_default;