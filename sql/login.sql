select "AuthorizedUsers".id, role_id, role 
from "AuthorizedUsers", "AuthorizedUsersRoles"
where "AuthorizedUsers". role_id = "AuthorizedUsersRoles".id
and login = :email
and password = :password
and is_active = true