insert into "AuthorizedUsers"(first_name, last_name, 
	patronymic, role_id, birthdate, passport_number, 
	passport_series, passport_date_of_issue, 
	taxpayer_registration_number, login, password, passport_authority_id, is_active)
values (:firstName, :lastName, :patronymic, :roleId, :birthdate, :number, :series, :dateOfIssue, :taxpayerRegistartionNumber, :email, :password, (select id from "PassportAuthorities" where code = :authorityCode), true)
returning id