select "AuthorizedUsers".*, "AuthorizedUsersRoles".role, "PassportAuthorities".code
from "AuthorizedUsers", "AuthorizedUsersRoles", "PassportAuthorities"
where "AuthorizedUsers".role_id = "AuthorizedUsersRoles".id
and "AuthorizedUsers".passport_authority_id = "PassportAuthorities".id
and "AuthorizedUsers".id = :id