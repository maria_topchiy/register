update "AuthorizedUsers"
set first_name=:firstName, last_name=:lastName, patronymic=:patronymic, 
role_id=:roleId, birthdate=:birthdate, passport_number=:number, passport_series=:series, 
passport_date_of_issue=:dateOfIssue, taxpayer_registration_number=:taxpayerRegistartionNumber, 
login=:email, password=:password, passport_authority_id=(select id from "PassportAuthorities" where code = :authorityCode),
is_active=:isActive, id=:id
where id = :id
returning *, (select "AuthorizedUsersRoles".role
from "AuthorizedUsersRoles"
where "AuthorizedUsersRoles".id = role_id), 
(select "PassportAuthorities".code
from "PassportAuthorities"
where "PassportAuthorities".id = passport_authority_id)