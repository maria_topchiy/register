select "AuthorizedUsers".*, "AuthorizedUsersRoles".role, "PassportAuthorities".code
from "AuthorizedUsers", "AuthorizedUsersRoles", "PassportAuthorities"
where "AuthorizedUsers".role_id = "AuthorizedUsersRoles".id
and "AuthorizedUsers".passport_authority_id = "PassportAuthorities".id
and first_name like :firstName
and last_name like :lastName
and patronymic like :patronymic
and role_id = :roleId
order by last_name, first_name, patronymic
offset :offset
limit :limit