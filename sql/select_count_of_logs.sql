select count (*)
from "Logs", "AuthorizedUsers" author, "AuthorizedUsers" affected, 
"AuthorizedUsersRoles" author_role, "AuthorizedUsersRoles" affected_role, "TypesOfChanges"
where "Logs".author_id = author.id
and "Logs".affected_user_id = affected.id
and author.role_id = author_role.id
and affected.role_id = affected_role.id
and "Logs".type_of_change_id = "TypesOfChanges".id
and author.first_name like :authorFirstName
and author.last_name like :authorLastName
and author.patronymic like :authorPatronymic
and author_role.id = :authorRoleId
and affected.first_name like :affectedUserFirstName
and affected.last_name like :affectedUserLastName
and affected.patronymic like :affectedUserPatronymic
and affected_role.id = :affectedUserRoleId
and "Logs".date = :date
and "Logs".type_of_change_id = :typeOfChangeId
and "Logs".request_id = :requestId
and exists (select *
from "Requests", "RequestOperationTypes"
where "Requests".operation_type_id = "RequestOperationTypes".id
and "Requests".id = "Logs".request_id
and "Requests".operation_type_id = :requestTypeId
and "Requests".date = :requestDate)