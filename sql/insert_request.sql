insert into "Requests"(operation_type_id, dismissal_order_date, 
	   dismissal_order_number, status_id, 
	   date, comment, commission_decision_number, 
	   commission_decision_date, commission_decision_type_id, 
	   certificate_id, commission, author_id, commission_authority_id)
values (:operationTypeId, :order_date, :order_number, 1, now(), null, :com_dec_number, :com_dec_date, :com_dec_typeId, :certificate_id, :commission, :authorId, :authorityId)
returning id