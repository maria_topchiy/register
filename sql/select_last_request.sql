select "Requests".*, "CommissionsDecisionsTypes".id com_dec_id, "CommissionsDecisionsTypes".type com_dec_type, "Authorities".id auth_id, "Authorities".name auth_name
from "Requests", "CommissionsDecisionsTypes", "Authorities"
where certificate_id = :id
and status_id = 2
and commission_decision_type_id = "CommissionsDecisionsTypes".id
and commission_authority_id = "Authorities".id
order by commission_decision_date desc
limit 1