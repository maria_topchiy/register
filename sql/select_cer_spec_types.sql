select "ExpertSpecialityTypes".id spec_id, index, "ExpertSpecialityTypes".name spec_name, "ExpertiseTypes".id expertise_id, expertise_number, "ExpertiseTypes".name expertise_name, class_number, "ExpertiseClasses".name class_name
from "CertificatesExpertSpecialityTypes", "ExpertSpecialityTypes", "ExpertiseTypes", "ExpertiseClasses"
where certificate_id = :id
and "ExpertSpecialityTypes".id = "CertificatesExpertSpecialityTypes".speciality_type_id
and "ExpertiseTypes".id = "ExpertSpecialityTypes".expertise_id
and "ExpertiseClasses".id = "ExpertiseTypes".class_id