select "Requests".id, "Requests".comment, "Requests".date, "RequestOperationTypes".operation_type
from "Requests", "RequestOperationTypes"
where "Requests".operation_type_id = "RequestOperationTypes".id
and "Requests".id = :id
and "Requests".operation_type_id = :requestTypeId
and "Requests".date = :requestDate