select count (*)
from "Experts"
where exists (
	select * 
	from "Certificates", "Requests", "CertificatesExpertSpecialityTypes", "ExpertSpecialityTypes", "ExpertiseTypes"
	where "Certificates".expert_id = "Experts".id
	and "Requests".certificate_id = "Certificates".id
	and "Requests".status_id = 2
	and "CertificatesExpertSpecialityTypes".certificate_id = "Certificates".id
	and "ExpertSpecialityTypes".id = "CertificatesExpertSpecialityTypes".speciality_type_id
	and "ExpertiseTypes".id = "ExpertSpecialityTypes".expertise_id
	and "ExpertiseTypes".class_id = any(:expertiseClassId::int[])
	and "ExpertSpecialityTypes".expertise_id = any(:expertiseTypeId::int[])
	and "ExpertSpecialityTypes".id = any(:expertiseSpecialityId::int[]))
and first_name like :firstName
and last_name like :lastName
and patronymic like :patronymic
and organization_region_id = :regionId
and is_from_government = :isFromGovernment
and authority_id = :authorityId
