select "Certificates".*, "Authorities".id auth_id, "Authorities".name auth_name
from "Certificates", "Experts", "Authorities"
where expert_id = :id
and "Experts".id = expert_id
and "Authorities".id = authority_id
and exists (
	select "Requests".id from "Requests"
	where "Requests".certificate_id = "Certificates".id
	and status_id = 2)