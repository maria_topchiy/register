select "Experts".*, "Authorities".name authority, "Regions".name region
from "Experts", "Authorities", "Regions"
where "Experts".authority_id = "Authorities".id
and "Experts".organization_region_id = "Regions".id
and "Experts".id = :id