insert into "Logs"(
author_id, type_of_change_id, date, affected_user_id, request_id)
values (:author_id, :type_of_change_id, now(), :affected_user_id, :request_id)