select "Requests".*, "RequestOperationTypes".operation_type, "RequestStatuses".status, "Certificates".expert_id,
"Certificates".document_number, "Certificates".validity_end_date,
com_auth.name com_auth_name,
"Experts".first_name ex_first_name, "Experts".last_name ex_last_name, "Experts".patronymic ex_patronymic, 
"Experts".organization_city, "Experts".organization_street, "Experts".organization_number_of_building, "Experts".organization,
"Experts".email, "Experts".phone, "Experts".is_from_government, ex_auth.name ex_auth_name, "Regions".name reg_name,
"AuthorizedUsers".first_name au_first_name, "AuthorizedUsers".last_name au_last_name, "AuthorizedUsers".patronymic au_patronymic, 
"AuthorizedUsers".role_id, "AuthorizedUsers".birthdate, "AuthorizedUsers".passport_number, "AuthorizedUsers".passport_series,
"AuthorizedUsers".passport_date_of_issue, "AuthorizedUsers".taxpayer_registration_number, "AuthorizedUsers".login, 
"AuthorizedUsers".password, "AuthorizedUsers".is_active, 
"AuthorizedUsersRoles".role, "PassportAuthorities".code
from "Requests", "RequestOperationTypes", "RequestStatuses", "Certificates", "Authorities" com_auth,
"Experts", "Regions", "Authorities" ex_auth, "AuthorizedUsers", "AuthorizedUsersRoles", "PassportAuthorities"
where "Requests".operation_type_id = "RequestOperationTypes".id
and "Requests".status_id = "RequestStatuses".id
and "Requests".certificate_id = "Certificates".id
and "Requests".commission_authority_id = com_auth.id
and "Certificates".expert_id = "Experts".id
and "Experts".authority_id = ex_auth.id
and "Experts".organization_region_id = "Regions".id
and "Requests".author_id = "AuthorizedUsers".id
and "AuthorizedUsers".role_id = "AuthorizedUsersRoles".id
and "AuthorizedUsers".passport_authority_id = "PassportAuthorities".id
and "Requests".id = :id
