select "Logs".*, author.first_name au_first_name, author.last_name au_last_name, author.patronymic au_patronymic, author.login au_login, 
affected.first_name af_first_name, affected.last_name af_last_name, affected.patronymic af_patronymic, affected.login af_login,
author_role.id au_role_id, author_role.role au_role, affected_role.id af_role_id, affected_role.role af_role, "TypesOfChanges".name
from "Logs", "AuthorizedUsers" author, "AuthorizedUsers" affected, 
"AuthorizedUsersRoles" author_role, "AuthorizedUsersRoles" affected_role, "TypesOfChanges"
where "Logs".author_id = author.id
and "Logs".affected_user_id = affected.id
and author.role_id = author_role.id
and affected.role_id = affected_role.id
and "Logs".type_of_change_id = "TypesOfChanges".id
and author.first_name like :authorFirstName
and author.last_name like :authorLastName
and author.patronymic like :authorPatronymic
and author_role.id = :authorRoleId
and affected.first_name like :affectedUserFirstName
and affected.last_name like :affectedUserLastName
and affected.patronymic like :affectedUserPatronymic
and affected_role.id = :affectedUserRoleId
and "Logs".date = :date
and "Logs".type_of_change_id = :typeOfChangeId
and "Logs".request_id = :requestId
order by date desc
offset :offset
limit :limit