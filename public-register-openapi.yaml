swagger: "2.0"
info:
  description: "This is an API for a server that should provide an access to the experts register"
  version: "1.0.0"
  title: "Experts Register"
host: "localhost:8090"
basePath: "/api"
tags:
- name: "Expert"
  description: "Everything regarding experts"
- name: "Expertise"
  description: "Everything regarding expertise (expertise classes, commissions, authorities etc.)"
 
schemes:
- "http"

paths:
  /api/experts:
    get:
      tags:
      - "Expert"
      summary: "Get all experts"
      description: "Get all experts with at least 1 valid certificate and that match provided parameters. Returns an empty array if no experts found"
      produces:
      - "application/json"
      parameters:
        - in: "query"
          name: "offset"
          type: "integer"
          description: "The number of experts to skip before starting to collect the result set. Experts with newer certificates should come first"
        - in: "query"
          name: "limit"
          type: "integer"
          description: "The number of experts to return"
        - in: "query"
          name: "firstName"
          type: "string"
          description: "The first name that experts should have"
        - in: "query"
          name: "lastName"
          type: "string"
          description: "The last name that experts should have"
        - in: "query"
          name: "patronymic"
          type: "string"
          description: "The patronymic that experts should have"
        - in: "query"
          name: "regionId"
          type: "string"
          description: "The ID of a region where experts work (place of work)"
        - in: "query"
          name: "isFromGovernment"
          type: "boolean"
          description: "Whether the experts should be from government"
        - in: "query"
          name: "authorityId"
          type: "string"
          description: "The ID of the authority that has given certificates to the experts"
        - in: "query"
          name: "expertiseSpecialityId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise specialities that experts should have"
        - in: "query"
          name: "expertiseTypeId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise types that experts should have"
        - in: "query"
          name: "expertiseClassId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise classes that experts should have"
      responses:
        "200":
          description: "Experts found"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Expert"
              
  /api/experts/totalCount:
    get:
        tags:
        - "Expert"
        summary: "Get total number of experts"
        description: "Get total number of experts with at least 1 valid certificate. Returns 0 if no experts found"
        parameters:
        - in: "query"
          name: "firstName"
          type: "string"
          description: "The first name that experts should have"
        - in: "query"
          name: "lastName"
          type: "string"
          description: "The last name that experts should have"
        - in: "query"
          name: "patronymic"
          type: "string"
          description: "The patronymic that experts should have"
        - in: "query"
          name: "regionId"
          type: "string"
          description: "The ID of a region where experts work (place of work)"
        - in: "query"
          name: "isFromGovernment"
          type: "boolean"
          description: "Whether the experts should be from government"
        - in: "query"
          name: "authorityId"
          type: "string"
          description: "The ID of the authority that has given certificates to the experts"
        - in: "query"
          name: "expertiseSpecialityId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise specialities that experts should have"
        - in: "query"
          name: "expertiseTypeId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise types that experts should have"
        - in: "query"
          name: "expertiseClassId"
          type: "array"
          items:
            type: "integer"
          description: "The IDs of the expertise classes that experts should have"
        produces:
        - "application/json"
        responses:
          "200":
            description: "Success"
            schema:
              type: "number"
 
  /api/experts/{id}:
    get:
      tags:
      - "Expert"
      summary: "Get an expert by ID"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "ID of the expert to get"
        required: true
        type: "integer"
      responses:
        "200":
          description: "Success"
          schema:
            $ref: "#/definitions/Expert"
        "404":
          description: "Not Found (no expert with this ID found)"
          schema:
            $ref: "#/definitions/Error"
            
  /api/experts/{id}/certificates:
    get:
        tags:
        - "Expert"
        summary: "Get all expert's certificates"
        description: "Get both valid and invalid certificates or an empty array if no certificates found"
        produces:
        - "application/json"
        parameters:
        - name: "id"
          in: "path"
          description: "ID of the expert"
          required: true
          type: "integer"
        responses:
          "200":
            description: "Success"
            schema:
              type: "array"
              items:
                $ref: "#/definitions/Certificate"
          "404":
            description: "Not Found (no expert with this ID found)"
            schema:
              $ref: "#/definitions/Error"
              
  /api/expertise/classes:
    get:
      tags:
        - "Expertise"
      summary: "Get all expertise classes"
      description: "Returns an array of expertise classes (empty array if nothing found)"
      produces:
      - "application/json"
      responses:
        "200":
          description: "Expertise classes found"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/ExpertiseClass"
              
  /api/expertise/authorities:
    get:
      tags:
        - "Expertise"
      summary: "Get all authorities"
      description: "Returns an array of authorities (empty array if nothing found)"
      produces:
      - "application/json"
      responses:
        "200":
          description: "Authorities found"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Authority"
        
  /api/expertise/commissions/decisions/types:
    get:
      tags:
        - "Expertise"
      summary: "Get all commission decision types"
      description: "Returns an array of decision types (empty array if nothing found)"
      produces:
      - "application/json"
      responses:
        "200":
          description: "Desicion types found"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/CommissionDecisionType"
    
definitions:

  Authority:
    type: "object"
    required:
    - "id"
    - "name"
    properties:
      id:
        type: "integer"
      name:
        type: "string"
        
  Certificate:
    type: "object"
    required:
    - "id"
    - "documentNumber"
    - "validityEndDate"
    - "expertiseSpecialities"
    - "expertiseType"
    - "commission"
    - "commissionDecision"
    properties:
      id:
        type: "integer"
      documentNumber:
        type: "integer"
      validityEndDate:
        type: "string"
        format: "date"
      expertiseSpecialities:
        type: "array"
        items:
          $ref: "#/definitions/ExpertiseSpeciality"
      expertiseType:
        $ref: "#/definitions/ExpertiseType"
      commission:
        $ref: "#/definitions/Commission"
      commissionDecision:
        $ref: "#/definitions/CommissionDecision"
      
  Commission:
    type: "object"
    required:
    - "name"
    - "authority"
    properties:
      name:
        type: "string"
      authority:
        $ref: "#/definitions/Authority"
      
  CommissionDecision:
    type: "object"
    required: 
    - "number"
    - "date"
    - "typeId"
    properties:
      number:
        type: "integer"
      date:
        type: "string"
        format: "date"
      type:
        $ref: "#/definitions/CommissionDecisionType"
        
  CommissionDecisionType:
    type: "object"
    required:
    - "id"
    - "type"
    properties:
      id:
        type: "integer"
      type:
        type: "string"
        example: "extending a certificate"
  
  Contacts:
    type: "object"
    required:
    - "phone"
    properties:
      email:
        type: "string"
        example: "john.brown@email.com"
      phone:
        type: "string"
        example: "12456789123"
        
  Expert:
    type: "object"
    required:
    - "id"
    - "firstName"
    - "lastName"
    - "contacts"
    - "organisation"
    - "isFromGovernment"
    properties:
      id:
        type: "integer"
        example: 1
      firstName:
        type: "string"
        example: "John"
      lastName:
        type: "string"
        example: "Brown"
      patronymic:
        type: "string"
      contacts:
        $ref: "#/definitions/Contacts"
      organisation: 
        $ref: "#/definitions/Organisation"
      isFromGovernment:
        type: "boolean"
        example: true
        
  ExpertiseClass:
    type: "object"
    required:
    - "id"
    - "classNumber"
    - "name"
    - "expertiseTypes"
    properties:
      id:
        type: "integer"
      classNumber:
        type: "integer"
      name:
        type: "string"
      expertiseTypes:
        type: "array"
        items: 
          $ref: "#/definitions/ExpertiseType"
        
  ExpertiseType:
    type: "object"
    required:
    - "id"
    - "number"
    - "name"
    - "expertiseSpecialities"
    properties:
      id:
        type: "integer"
      number:
        type: "integer"
      name:
        type: "string"
      expertiseSpecialities:
        type: "array"
        items: 
          $ref: "#/definitions/ExpertiseSpeciality"
      
  ExpertiseSpeciality:
    type: "object"
    required:
    - "id"
    - "number"
    - "name"
    properties:
      id:
        type: "integer"
      number:
        type: "integer"
      name:
        type: "string"
        
  Error:
    type: "object"
    required:
    - "message"
    properties:
      message:
        type: "string"
        description: "Error message"
        
  Organisation:
    type: "object"
    required:
    - "name"
    - "region"
    - "city"
    - "street"
    - "numberOfBuilding"
    properties:
      name:
        type: "string"
        description: "Name of an organisation where the expert works"
      region:
        type: "string"
      city:
        type: "string"
        example: "London"
      street:
        type: "string"
        example: "Main Str."
      numberOfBuilding:
        type: "integer"
        example: 1