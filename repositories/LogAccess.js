const TypeOfChange = require('../models/type_of_change');
const Request = require('../models/request');
const User = require('../models/user');
const UserRole = require('../models/user_role');
const Log = require('../models/log');
const fs = require('fs');
const dbStorage = require('../dbStorage');
 
class LogAccess {
 
    constructor() 
    {
        this.storage = dbStorage;
    }
 
    async getLogs(query)
    {
        let select_logs = fs.readFileSync('./sql/select_logs.sql').toString();
        let select_log_request = fs.readFileSync('./sql/select_log_request.sql').toString();

        if(query['authorFirstName'] == undefined) query['authorFirstName'] = '';
        if(query['authorLastName'] == undefined) query['authorLastName'] = '';
        if(query['authorPatronymic'] == undefined) query['authorPatronymic'] = '';
        if(query['affectedUserFirstName'] == undefined) query['affectedUserFirstName'] = '';
        if(query['affectedUserLastName'] == undefined) query['affectedUserLastName'] = '';
        if(query['affectedUserPatronymic'] == undefined) query['affectedUserPatronymic'] = '';
        if(query['authorRoleId'] == undefined) {
            select_logs = select_logs.replace("and author_role.id = :authorRoleId", '');
        }
        if(query['affectedUserRoleId'] == undefined) {
            select_logs = select_logs.replace("and affected_role.id = :affectedUserRoleId", '');
        }
        if(query['date'] == undefined) {
            select_logs = select_logs.replace('and "Logs".date = :date', '');
        }
        if(query['typeOfChangeId'] == undefined) {
            select_logs = select_logs.replace('and "Logs".type_of_change_id = :typeOfChangeId', '');
        }
        if(query['requestId'] == undefined) {
            select_logs = select_logs.replace('and "Logs".request_id = :requestId', '');
        }
        if(query['requestTypeId'] == undefined) {
            select_log_request = select_log_request.replace('and "Requests".operation_type_id = :requestTypeId', '');
        }
        if(query['requestDate'] == undefined) {
            select_log_request = select_log_request.replace('and "Requests".date = :requestDate', '');
        }
    
        const values = {offset: query['offset'], 
                        limit: query['limit'], 
                        authorFirstName: '%'+query['authorFirstName']+'%', 
                        authorLastName: '%'+query['authorLastName']+'%', 
                        authorPatronymic: '%'+query['authorPatronymic']+'%', 
                        authorRoleId: query['authorRoleId'],
                        affectedUserFirstName: '%'+query['affectedUserFirstName']+'%', 
                        affectedUserLastName: '%'+query['affectedUserLastName']+'%',
                        affectedUserPatronymic: '%'+query['affectedUserPatronymic']+'%',
                        affectedUserRoleId: query['affectedUserRoleId'],
                        date: query['date'],
                        typeOfChangeId: query['typeOfChangeId'],
                        requestId: query['requestId']}

        let items = await this.storage.readItems(select_logs, values);
        let items2 = [];
        for(const item of items) {
            const values2 = {id: item["request_id"],
                            requestTypeId: query['requestTypeId'],
                            requestDate: query['requestDate']}

            let req_items = await this.storage.readItems(select_log_request, values2);
            let req_item = req_items[0];
            let request;
            if(req_item == null) {
                if(query['requestTypeId'] == undefined && query['requestDate'] == undefined) {
                    request = null; 
                }
                else {
                    continue;
                }
            } 
            else {
                request = new Request(req_item["id"], req_item["date"], req_item["comment"], req_item["operation_type"]);
            }
            
            const type = new TypeOfChange(item["type_of_change_id"], item["name"]);
            const af_role = new UserRole(item["af_role_id"], item["af_role"]);
            const affected = new User(item["affected_user_id"], af_role, item["af_first_name"], item["af_last_name"], item["af_patronymic"], item["af_login"]);
            const au_role = new UserRole(item["au_role_id"], item["au_role"]);
            const author = new User(item["author_id"], au_role, item["au_first_name"], item["au_last_name"], item["au_patronymic"], item["au_login"]);
            const log = new Log(item["id"], author, affected, item["date"], type, request);
            items2.push(log);
        }
        return items2;
    }

    getLogTotalCount(query) 
    {
        let select_count_of_logs = fs.readFileSync('./sql/select_count_of_logs.sql').toString();

        if(query['authorFirstName'] == undefined) query['authorFirstName'] = '';
        if(query['authorLastName'] == undefined) query['authorLastName'] = '';
        if(query['authorPatronymic'] == undefined) query['authorPatronymic'] = '';
        if(query['affectedUserFirstName'] == undefined) query['affectedUserFirstName'] = '';
        if(query['affectedUserLastName'] == undefined) query['affectedUserLastName'] = '';
        if(query['affectedUserPatronymic'] == undefined) query['affectedUserPatronymic'] = '';
        if(query['authorRoleId'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace("and author_role.id = :authorRoleId", '');
        }
        if(query['affectedUserRoleId'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace("and affected_role.id = :affectedUserRoleId", '');
        }
        if(query['date'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace('and "Logs".date = :date', '');
        }
        if(query['typeOfChangeId'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace('and "Logs".type_of_change_id = :typeOfChangeId', '');
        }
        if(query['requestId'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace('and "Logs".request_id = :requestId', '');
        }
        if(query['requestTypeId'] == undefined && query['requestDate'] == undefined) {
            select_count_of_logs = select_count_of_logs.replace('and exists (select *\nfrom "Requests", "RequestOperationTypes"\nwhere "Requests".operation_type_id = "RequestOperationTypes".id\nand "Requests".id = "Logs".request_id\nand "Requests".operation_type_id = :requestTypeId\nand "Requests".date = :requestDate)', ''); 
        }
        else {
            if(query['requestTypeId'] == undefined) {
                select_count_of_logs = select_count_of_logs.replace('and "Requests".operation_type_id = :requestTypeId', '');
            }
            if(query['requestDate'] == undefined) {
                select_count_of_logs = select_count_of_logs.replace('and "Requests".date = :requestDate', '');
            }
        }
        
        const values = {offset: query['offset'], 
                        limit: query['limit'], 
                        authorFirstName: '%'+query['authorFirstName']+'%', 
                        authorLastName: '%'+query['authorLastName']+'%', 
                        authorPatronymic: '%'+query['authorPatronymic']+'%', 
                        authorRoleId: query['authorRoleId'],
                        affectedUserFirstName: '%'+query['affectedUserFirstName']+'%', 
                        affectedUserLastName: '%'+query['affectedUserLastName']+'%',
                        affectedUserPatronymic: '%'+query['affectedUserPatronymic']+'%',
                        affectedUserRoleId: query['affectedUserRoleId'],
                        date: query['date'],
                        typeOfChangeId: query['typeOfChangeId'],
                        requestId: query['requestId'],
                        requestTypeId: query['requestTypeId'],
                        requestDate: query['requestDate']}


        let count = this.storage.readItems(select_count_of_logs, values);
        return count.then(count => {
            return count[0]["count"];
        });
    }

    getTypesOfChanges() 
    {
        let select_types_of_changes = fs.readFileSync('./sql/select_types_of_changes.sql').toString();
        
        let items = this.storage.readItems(select_types_of_changes, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const type = new TypeOfChange(item["id"], item["name"]);
                items2.push(type);
            }
            return items2;
        });
    }

    addLog(author_id, type_of_change_id, affected_user_id, request_id) {
        let insert_log = fs.readFileSync('./sql/insert_log.sql').toString();

        
        const values = {author_id: author_id, 
                        type_of_change_id: type_of_change_id, 
                        affected_user_id: affected_user_id, 
                        request_id: request_id}

        this.storage.readItems(insert_log, values);
    }
};
 
module.exports = LogAccess;

