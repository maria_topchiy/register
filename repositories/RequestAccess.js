const Expert = require('../models/expert');
const Contacts = require('../models/contacts');
const Organisation = require('../models/organisation');
const Certificate = require('../models/certificate');
const ExpertiseSpeciality = require('../models/expertise_speciality');
const Commission = require('../models/commission');
const Authority = require('../models/authority');
const CommissionDecision = require('../models/commission_decision');
const CommissionDecisionType = require('../models/commission_decision_type');
const RequestStatus = require('../models/request_status');
const RequestOperationType = require('../models/request_operation_type');
const Request = require('../models/request');
const User = require('../models/user');
const UserRole = require('../models/user_role');
const PassportData = require('../models/passport_data');
const fs = require('fs');
const dbStorage = require('../dbStorage');
 
class RequestAccess {
 
    constructor() 
    {
        this.storage = dbStorage;
    }
 
    select_cer_spec_types(values)
    {
        let select_cer_spec_types = fs.readFileSync('./sql/select_cer_spec_types.sql').toString();


        let items = this.storage.readItems(select_cer_spec_types, values);
        return items.then(items => {
            const ex_type = new ExpertiseSpeciality(items[0]["expertise_id"], items[0]["expertise_number"], items[0]["expertise_name"]);
            let items2 = [];
            for(const item of items) {
                const expertise_speciality = new ExpertiseSpeciality(item["spec_id"], item["index"], item["spec_name"]);
                items2.push(expertise_speciality);
            }
            return [items2, ex_type];
        });

    }

    async getRequests(offset, limit, authorId, statusId) 
    {
        let select_requests = fs.readFileSync('./sql/select_requests.sql').toString();
        let select_com_dec_type = fs.readFileSync('./sql/select_com_dec_type.sql').toString();

        if(authorId == undefined) {
            select_requests = select_requests.replace("and author_id = :authorId", '');
        }
        if(statusId == undefined) {
            select_requests = select_requests.replace("and status_id = :statusId", '');
        }
        if(limit == undefined) {
            select_requests = select_requests.replace("limit :limit", '');
        }
        if(offset == undefined) {
            select_requests = select_requests.replace("offset :offset", '');
        }

        const values = {offset: offset, 
                        limit: limit, 
                        authorId: authorId, 
                        statusId: statusId}

        let items = await this.storage.readItems(select_requests, values);
        let items2 = [];

        let dec_type = null;

        for(const item of items) {
            const values2 = {id: item["certificate_id"]};
            let spec_ex_type = await this.select_cer_spec_types(values2);

            if(item["commission_decision_type_id"] != null) {
                const values3 = {id: item["commission_decision_type_id"]};
                let dec_types = await this.storage.readItems(select_com_dec_type, values3);
                dec_type = dec_types[0]['type'];
            }
            
            const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
            const role = new UserRole(item["role_id"], item["role"]);
            const author = new User(item["author_id"], role, item["au_first_name"], item["au_last_name"], item["au_patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
            const com_dec_type = new CommissionDecisionType(item["commission_decision_type_id"], dec_type);
            const com_dec = new CommissionDecision(item["commission_decision_number"], item["commission_decision_date"], com_dec_type);
            const authority = new Authority(item["commission_authority_id"], item["com_auth_name"]);
            const commission = new Commission(item["commission"], authority);
            const contacts = new Contacts(item["email"], item["phone"]);
            const organisation = new Organisation(item["organization"], item["region"], item["organization_city"], item["organization_street"], item["organization_number_of_building"]);
            const expert = new Expert(item["expert_id"], item["ex_first_name"], item["ex_last_name"], item["ex_patronymic"], contacts, organisation, item["ex_auth_name"], item["is_from_government"]);
            const certificate = new Certificate(item["certificate_id"], item["document_number"], item["validity_end_date"], spec_ex_type[0], spec_ex_type[1], commission, com_dec);
            const status = new RequestStatus(item["status_id"], item["status"]);
            const op_type = new RequestOperationType(item["operation_type_id"], item["operation_type"]);
            const request = new Request(item["id"], item["date"], item["comment"], op_type, status, certificate, expert, commission, com_dec, item["dismissal_order_date"], item["dismissal_order_number"], author);
            items2.push(request);
        }
        return items2;
        
    }

    async getRequestById(requestId) 
    {
        let select_request = fs.readFileSync('./sql/select_request.sql').toString();
        let select_com_dec_type = fs.readFileSync('./sql/select_com_dec_type.sql').toString();

        const values = {id: requestId}

        let items = await this.storage.readItems(select_request, values);
        let item = items[0];
        if(item == null) return item;

        const values2 = {id: item["certificate_id"]};
        let spec_ex_type = await this.select_cer_spec_types(values2);

        let dec_type = null;

        if(item["commission_decision_type_id"] != null) {
            const values3 = {id: item["commission_decision_type_id"]};
            let dec_types = await this.storage.readItems(select_com_dec_type, values3);
            dec_type = dec_types[0]['type'];
        }

        const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
        const role = new UserRole(item["role_id"], item["role"]);
        const author = new User(item["author_id"], role, item["au_first_name"], item["au_last_name"], item["au_patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
        const com_dec_type = new CommissionDecisionType(item["commission_decision_type_id"], dec_type);
        const com_dec = new CommissionDecision(item["commission_decision_number"], item["commission_decision_date"], com_dec_type);
        const authority = new Authority(item["commission_authority_id"], item["com_auth_name"]);
        const commission = new Commission(item["commission"], authority);
        const contacts = new Contacts(item["email"], item["phone"]);
        const organisation = new Organisation(item["organization"], item["region"], item["organization_city"], item["organization_street"], item["organization_number_of_building"]);
        const expert = new Expert(item["expert_id"], item["ex_first_name"], item["ex_last_name"], item["ex_patronymic"], contacts, organisation, item["ex_auth_name"], item["is_from_government"]);
        const certificate = new Certificate(item["certificate_id"], item["document_number"], item["validity_end_date"], spec_ex_type[0], spec_ex_type[1], commission, com_dec);
        const status = new RequestStatus(item["status_id"], item["status"]);
        const op_type = new RequestOperationType(item["operation_type_id"], item["operation_type"]);
        const request = new Request(item["id"], item["date"], item["comment"], op_type, status, certificate, expert, commission, com_dec, item["dismissal_order_date"], item["dismissal_order_number"], author);
        return request;
    }

    getRequestTotalCount(authorId, statusId) 
    {
        let select_count_of_requests = fs.readFileSync('./sql/select_count_of_requests.sql').toString();
    
        if(authorId == undefined) {
            select_count_of_requests = select_count_of_requests.replace("and author_id = :authorId", '');
        }
        if(statusId == undefined) {
            select_count_of_requests = select_count_of_requests.replace("and status_id = :statusId", '');
        }

        const values = {authorId: authorId, 
                        statusId: statusId}


        let count = this.storage.readItems(select_count_of_requests, values);
        return count.then(count => {
            return count[0]["count"];
        });
    }

    async updateRequest(id, body)
    {
        let update_request = fs.readFileSync('./sql/update_request.sql').toString();

        const values = {id: id,
                        statusId: body['statusId'],
                        comment: body['comment']}

        let items = await this.storage.readItems(update_request, values);
        let item = items[0];
        if(item == null) return item;

        const values2 = {id: item["certificate_id"]};
        let spec_ex_type = await this.select_cer_spec_types(values2);

        const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
        const role = new UserRole(item["role_id"], item["role"]);
        const author = new User(item["author_id"], role, item["au_first_name"], item["au_last_name"], item["au_patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
        const com_dec_type = new CommissionDecisionType(item["commission_decision_type_id"], item["dec_type"]);
        const com_dec = new CommissionDecision(item["commission_decision_number"], item["commission_decision_date"], com_dec_type);
        const authority = new Authority(item["commission_authority_id"], item["com_auth_name"]);
        const commission = new Commission(item["commission"], authority);
        const contacts = new Contacts(item["email"], item["phone"]);
        const organisation = new Organisation(item["organization"], item["region"], item["organization_city"], item["organization_street"], item["organization_number_of_building"]);
        const expert = new Expert(item["expert_id"], item["ex_first_name"], item["ex_last_name"], item["ex_patronymic"], contacts, organisation, item["ex_auth_name"], item["is_from_government"]);
        const certificate = new Certificate(item["certificate_id"], item["document_number"], item["validity_end_date"], spec_ex_type[0], spec_ex_type[1], commission, com_dec);
        const status = new RequestStatus(item["status_id"], item["status"]);
        const op_type = new RequestOperationType(item["operation_type_id"], item["operation_type"]);
        const request = new Request(item["id"], item["date"], item["comment"], op_type, status, certificate, expert, commission, com_dec, item["dismissal_order_date"], item["dismissal_order_number"], author);
        return request;
    }

    async addRequest(body, token)
    {
        let insert_request = fs.readFileSync('./sql/insert_request.sql').toString();
        let insert_certificate = fs.readFileSync('./sql/insert_certificate.sql').toString();
        let insert_expert = fs.readFileSync('./sql/insert_expert.sql').toString();
        let insert_cer_spec_type = fs.readFileSync('./sql/insert_cer_spec_type.sql').toString();

        let ex_id;
        let cer_id;

        if(body['operationTypeId'] == 1) {

            if(body.expert['id'] == undefined) {
                const values = {firstName: body.expert['firstName'],
                                lastName: body.expert['lastName'],
                                patronymic: body.expert['patronymic'],
                                email: body.expert.contacts['email'],
                                phone: body.expert.contacts['phone'],
                                orgName: body.expert.organisation['name'],
                                regionId: body.expert.organisation['regionId'],
                                city: body.expert.organisation['city'],
                                street: body.expert.organisation['street'],
                                numberOfBuilding: body.expert.organisation['numberOfBuilding'],
                                authorityId: body.commission['authorityId']};

                let expert = await this.storage.readItems(insert_expert, values);
                ex_id = expert[0]["id"];
            }
            else {
                ex_id = body.expert['id'];
            }
            
            const values2 = {documentNumber: body.certificate['documentNumber'],
                            validityEndDate: body.certificate['validityEndDate'],
                            expert_id: ex_id};

            let certificate = await this.storage.readItems(insert_certificate, values2);
            cer_id = certificate[0]["id"];
            
            let ex_spec_id = body.certificate['expertiseSpecialitiesId'];
            
            for(let i = 0; i < ex_spec_id.length; i++) {
                const values3 = {certificate_id: cer_id,
                                expertiseSpecialityId: ex_spec_id[i]};

                await this.storage.readItems(insert_cer_spec_type, values3);
            }
        
        }
        else {
            cer_id = body.certificate['id'];
        }

        let values4;
        if(token['role_id'] == 4) {
            values4 = {operationTypeId: body['operationTypeId'],
                        certificate_id: cer_id,
                        commission: body.commission['name'],
                        authorityId: body.commission['authorityId'],
                        com_dec_number: body.commissionDecision['number'],
                        com_dec_date: body.commissionDecision['date'],
                        com_dec_typeId: null,
                        order_date: null,
                        order_number: null,
                        authorId: body['authorId']};
        }
        else {
            values4 = {operationTypeId: body['operationTypeId'],
                        certificate_id: cer_id,
                        commission: body.commission['name'],
                        authorityId: body.commission['authorityId'],
                        com_dec_number: body.commissionDecision['number'],
                        com_dec_date: body.commissionDecision['date'],
                        com_dec_typeId: body.commissionDecision['typeId'],
                        order_date: body.dismissalOrder['date'],
                        order_number: body.dismissalOrder['number'],
                        authorId: body['authorId']};
        }

        let request = await this.storage.readItems(insert_request, values4);
        return request[0];
    }

    getStatuses() 
    {
        let select_statuses = fs.readFileSync('./sql/select_statuses.sql').toString();
        
        let items = this.storage.readItems(select_statuses, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const status = new RequestStatus(item["id"], item["status"]);
                items2.push(status);
            }
            return items2;
        });
    }

    getOperationTypes() 
    {
        let select_operation_types = fs.readFileSync('./sql/select_operation_types.sql').toString();
        
        let items = this.storage.readItems(select_operation_types, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const type = new RequestOperationType(item["id"], item["operation_type"]);
                items2.push(type);
            }
            return items2;
        });
    }
};
 
module.exports = RequestAccess;

