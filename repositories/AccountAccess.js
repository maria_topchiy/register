const User = require('../models/user');
const UserRole = require('../models/user_role');
const PassportData = require('../models/passport_data');
const Login = require('../models/login');
const fs = require('fs');
const dbStorage = require('../dbStorage');
const crypto = require('crypto'); 
const jwt = require('jsonwebtoken');
 
class AccountAccess {
 
    constructor() 
    {
        this.storage = dbStorage;
    }
 
    getUsers(offset, limit, firstName, lastName, patronymic, roleId) 
    {
        let select_users = fs.readFileSync('./sql/select_users.sql').toString();

        if(firstName == undefined) firstName = '';
        if(lastName == undefined) lastName = '';
        if(patronymic == undefined) patronymic = '';
        if(roleId == undefined) {
            select_users = select_users.replace("and role_id = :roleId", '');
        }
        
        const values = {offset: offset, 
                        limit: limit, 
                        firstName: '%'+firstName+'%', 
                        lastName: '%'+lastName+'%', 
                        patronymic: '%'+patronymic+'%', 
                        roleId: roleId}


        let items = this.storage.readItems(select_users, values);
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
                const role = new UserRole(item["role_id"], item["role"]);
                const user = new User(item["id"], role, item["first_name"], item["last_name"], item["patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
                items2.push(user);
            }
            return items2;
        });
    }

    getUserById(userId) 
    {
        let select_user = fs.readFileSync('./sql/select_user.sql').toString();

        const values = {id: userId}

        let items = this.storage.readItems(select_user, values);
        return items.then(items => {
            let item = items[0];
            if(item == null) return item;

            const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
            const role = new UserRole(item["role_id"], item["role"]);
            const user = new User(item["id"], role, item["first_name"], item["last_name"], item["patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
            return user;
        });

    }

    getUserTotalCount(firstName, lastName, patronymic, roleId) 
    {
        let select_count_of_users = fs.readFileSync('./sql/select_count_of_users.sql').toString();

        if(firstName == undefined) firstName = '';
        if(lastName == undefined) lastName = '';
        if(patronymic == undefined) patronymic = '';
        if(roleId == undefined) {
            select_count_of_users = select_count_of_users.replace("and role_id = :roleId", '');
        }
        
        const values = {firstName: '%'+firstName+'%', 
                        lastName: '%'+lastName+'%', 
                        patronymic: '%'+patronymic+'%', 
                        roleId: roleId}


        let count = this.storage.readItems(select_count_of_users, values);
        return count.then(count => {
            return count[0]["count"];
        });

    }

    getUserRoles() 
    {
        let select_roles = fs.readFileSync('./sql/select_roles.sql').toString();
        
        let items = this.storage.readItems(select_roles, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const role = new UserRole(item["id"], item["role"]);
                items2.push(role);
            }
            return items2;
        });
    }

    updateUser(id, body)
    {
        let update_user = fs.readFileSync('./sql/update_user.sql').toString();

        if(body['firstName'] == undefined) {
            update_user = update_user.replace("first_name=:firstName,", '');
        }
        if(body['lastName'] == undefined) {
            update_user = update_user.replace("last_name=:lastName,", '');
        }
        if(body['patronymic'] == undefined) {
            update_user = update_user.replace("patronymic=:patronymic,", '');
        }
        if(body['roleId'] == undefined) {
            update_user = update_user.replace("role_id=:roleId,", '');
        }
        if(body['birthdate'] == undefined) {
            update_user = update_user.replace("birthdate=:birthdate,", '');
        }
        if(body.passport['number'] == undefined) {
            update_user = update_user.replace("passport_number=:number,", '');
        }
        if(body.passport['series'] == undefined) {
            update_user = update_user.replace("passport_series=:series,", '');
        }
        if(body.passport['dateOfIssue'] == undefined) {
            update_user = update_user.replace("passport_date_of_issue=:dateOfIssue,", '');
        }
        if(body.passport['authorityCode'] == undefined) {
            update_user = update_user.replace('passport_authority_id=(select id from "PassportAuthorities" where code = :authorityCode),', '');
        }
        if(body['taxpayerRegistartionNumber'] == undefined) {
            update_user = update_user.replace("taxpayer_registration_number=:taxpayerRegistartionNumber,", '');
        }
        if(body['email'] == undefined) {
            update_user = update_user.replace("login=:email,", '');
        }
        let password;
        if(body['password'] == undefined) {
            update_user = update_user.replace("password=:password,", '');
        }
        else {
            password = crypto.createHash("sha256").update(body['password']).digest("hex");
        }
        if(body['isActive'] == undefined) {
            update_user = update_user.replace("is_active=:isActive,", '');
        }

        const values = {id: id,
                        roleId: body['roleId'],
                        firstName: body['firstName'], 
                        lastName: body['lastName'], 
                        patronymic: body['patronymic'], 
                        birthdate: body['birthdate'],
                        number: body.passport['number'],
                        series: body.passport['series'],
                        dateOfIssue: body.passport['dateOfIssue'],
                        authorityCode: body.passport['authorityCode'],
                        taxpayerRegistartionNumber: body['taxpayerRegistartionNumber'],
                        email: body['email'],
                        password: password,
                        isActive: body['isActive']}

        let items = this.storage.readItems(update_user, values);
        return items.then(items => {
            let item = items[0];
            if(item == null) return item;

            const passport = new PassportData(item["passport_number"], item["passport_series"], item["passport_date_of_issue"], item["code"]);
            const role = new UserRole(item["role_id"], item["role"]);
            const user = new User(item["id"], role, item["first_name"], item["last_name"], item["patronymic"], item["birthdate"], passport, item["taxpayer_registration_number"], item["login"], item["password"], item["is_active"]);
            return user;
        });

    }

    register(body)
    {
        let registration = fs.readFileSync('./sql/registration.sql').toString();

        let password = crypto.createHash("sha256").update(body['password']).digest("hex");

        const values = {roleId: body['roleId'],
                        firstName: body['firstName'], 
                        lastName: body['lastName'], 
                        patronymic: body['patronymic'], 
                        birthdate: body['birthdate'],
                        number: body.passport['number'],
                        series: body.passport['series'],
                        dateOfIssue: body.passport['dateOfIssue'],
                        authorityCode: body.passport['authorityCode'],
                        taxpayerRegistartionNumber: body['taxpayerRegistartionNumber'],
                        email: body['email'],
                        password: password}

        let items = this.storage.readItems(registration, values);
        return items.then(items => {
            let item = items[0];
            return item;
        });

    }

    login(body)
    {
        let login = fs.readFileSync('./sql/login.sql').toString();

        let password = crypto.createHash("sha256").update(body['password']).digest("hex");

        const values = {email: body['email'],
                        password: password}

        let items = this.storage.readItems(login, values);
        return items.then(items => {
            let item = items[0];
            if(item == null) return item;

            const secret = 'tynyjhrdge';
            const token = jwt.sign({"id": item["id"], "role_id": item["role_id"]}, secret);
            
            const role = new UserRole(item["role_id"], item["role"]);
            const login = new Login(token, role, item["id"]);
            return login;
        });

    }
};
 
module.exports = AccountAccess;

