const Expert = require('../models/expert');
const Contacts = require('../models/contacts');
const Organisation = require('../models/organisation');
const Certificate = require('../models/certificate');
const ExpertiseClass = require('../models/expertise_class');
const ExpertiseType = require('../models/expertise_type');
const ExpertiseSpeciality = require('../models/expertise_speciality');
const Commission = require('../models/commission');
const Authority = require('../models/authority');
const CommissionDecision = require('../models/commission_decision');
const CommissionDecisionType = require('../models/commission_decision_type');
const Region = require('../models/region');
const fs = require('fs');
const dbStorage = require('../dbStorage');
 
class RegisterAccess {
 
    constructor() 
    {
        this.storage = dbStorage;
    }
 
    getExperts(offset, limit, firstName, lastName, patronymic, regionId, isFromGovernment, authorityId, expertiseSpecialityId, expertiseTypeId, expertiseClassId) 
    {
        let select_experts = fs.readFileSync('./sql/select_experts.sql').toString();

        if(firstName == undefined) firstName = '';
        if(lastName == undefined) lastName = '';
        if(patronymic == undefined) patronymic = '';
        if(regionId == undefined) {
            select_experts = select_experts.replace("and organization_region_id = :regionId", '');
        }
        if(isFromGovernment == undefined) {
            select_experts = select_experts.replace("and is_from_government = :isFromGovernment", '');
        }
        if(authorityId == undefined) {
            select_experts = select_experts.replace("and authority_id = :authorityId", '');
        }
        if(expertiseSpecialityId == undefined) {
            select_experts = select_experts.replace('and "ExpertSpecialityTypes".id = any(:expertiseSpecialityId::int[])', '');
        }
        else {
            expertiseSpecialityId = expertiseSpecialityId.split(',');
            for(let i = 0; i < expertiseSpecialityId.length; i++) {
                expertiseSpecialityId[i] = parseInt(expertiseSpecialityId[i]);
            }
        }
        if(expertiseTypeId == undefined) {
            select_experts = select_experts.replace('and "ExpertSpecialityTypes".expertise_id = any(:expertiseTypeId::int[])', '');
        }
        else {
            expertiseTypeId = expertiseTypeId.split(',');
            for(let i = 0; i < expertiseTypeId.length; i++) {
                expertiseTypeId[i] = parseInt(expertiseTypeId[i]);
            }
        }
        let expertiseClassIds = [];
        if(expertiseClassId == undefined) {
            select_experts = select_experts.replace('and "ExpertiseTypes".class_id = any(:expertiseClassId::int[])', '');
        }
        else {
            if(expertiseClassId.length == 1) {
                expertiseClassIds[0] = parseInt(expertiseClassId);
            }
            else {
                for(let i = 0; i < expertiseClassId.length; i++) {
                    expertiseClassIds[i] = parseInt(expertiseClassId[i]);
                }
            }
        }
       
        const values = {offset: offset, 
                        limit: limit, 
                        firstName: '%'+firstName+'%', 
                        lastName: '%'+lastName+'%', 
                        patronymic: '%'+patronymic+'%', 
                        regionId: regionId,
                        isFromGovernment: isFromGovernment, 
                        authorityId: authorityId,
                        expertiseSpecialityId: expertiseSpecialityId,
                        expertiseTypeId: expertiseTypeId,
                        expertiseClassId: expertiseClassIds}

        let items = this.storage.readItems(select_experts, values);
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const contacts = new Contacts(item["email"], item["phone"]);
                const organisation = new Organisation(item["organization"], item["region"], item["organization_city"], item["organization_street"], item["organization_number_of_building"]);
                const expert = new Expert(item["id"], item["first_name"], item["last_name"], item["patronymic"], contacts, organisation, item["authority"], item["is_from_government"]);
                items2.push(expert);
            }
            return items2;
        });
    }

    getExpertById(expertId) 
    {
        let select_expert = fs.readFileSync('./sql/select_expert.sql').toString();

        const values = {id: expertId}

        let items = this.storage.readItems(select_expert, values);
        return items.then(items => {
            let item = items[0];
            if(item == null) return item;

            const contacts = new Contacts(item["email"], item["phone"]);
            const organisation = new Organisation(item["organization"], item["region"], item["organization_city"], item["organization_street"], item["organization_number_of_building"]);
            const expert = new Expert(item["id"], item["first_name"], item["last_name"], item["patronymic"], contacts, organisation, item["authority"], item["is_from_government"]);
            return expert;
        });
    }

    getExpertTotalCount(firstName, lastName, patronymic, regionId, isFromGovernment, authorityId, expertiseSpecialityId, expertiseTypeId, expertiseClassId) 
    {
        let select_count_of_experts = fs.readFileSync('./sql/select_count_of_experts.sql').toString();

        if(firstName == undefined) firstName = '';
        if(lastName == undefined) lastName = '';
        if(patronymic == undefined) patronymic = '';
        if(regionId == undefined) {
            select_count_of_experts = select_count_of_experts.replace("and organization_region_id = :regionId", '');
        }
        if(isFromGovernment == undefined) {
            select_count_of_experts = select_count_of_experts.replace("and is_from_government = :isFromGovernment", '');
        }
        if(authorityId == undefined) {
            select_count_of_experts = select_count_of_experts.replace("and authority_id = :authorityId", '');
        }
        if(expertiseSpecialityId == undefined) {
            select_count_of_experts = select_count_of_experts.replace('and "ExpertSpecialityTypes".id = any(:expertiseSpecialityId::int[])', '');
        }
        else {
            expertiseSpecialityId = expertiseSpecialityId.split(',');
            for(let i = 0; i < expertiseSpecialityId.length; i++) {
                expertiseSpecialityId[i] = parseInt(expertiseSpecialityId[i]);
            }
        }
        if(expertiseTypeId == undefined) {
            select_count_of_experts = select_count_of_experts.replace('and "ExpertSpecialityTypes".expertise_id = any(:expertiseTypeId::int[])', '');
        }
        else {
            expertiseTypeId = expertiseTypeId.split(',');
            for(let i = 0; i < expertiseTypeId.length; i++) {
                expertiseTypeId[i] = parseInt(expertiseTypeId[i]);
            }
        }
        let expertiseClassIds = [];
        if(expertiseClassId == undefined) {
            select_count_of_experts = select_count_of_experts.replace('and "ExpertiseTypes".class_id = any(:expertiseClassId::int[])', '');
        }
        else {
            if(expertiseClassId.length == 1) {
                expertiseClassIds[0] = parseInt(expertiseClassId);
            }
            else {
                for(let i = 0; i < expertiseClassId.length; i++) {
                    expertiseClassIds[i] = parseInt(expertiseClassId[i]);
                }
            }
        }

        const values = {firstName: '%'+firstName+'%', 
                        lastName: '%'+lastName+'%', 
                        patronymic: '%'+patronymic+'%', 
                        regionId: regionId,
                        isFromGovernment: isFromGovernment, 
                        authorityId: authorityId,
                        expertiseSpecialityId: expertiseSpecialityId,
                        expertiseTypeId: expertiseTypeId,
                        expertiseClassId: expertiseClassIds}

        let count = this.storage.readItems(select_count_of_experts, values);
        return count.then(count => {
            return count[0]["count"];
        });
    }

    select_cer_spec_types(values)
    {
        let select_cer_spec_types = fs.readFileSync('./sql/select_cer_spec_types.sql').toString();

        let items = this.storage.readItems(select_cer_spec_types, values);
        return items.then(items => {
            const ex_type = new ExpertiseSpeciality(items[0]["expertise_id"], items[0]["expertise_number"], items[0]["expertise_name"]);
            let items2 = [];
            for(const item of items) {
                const expertise_speciality = new ExpertiseSpeciality(item["spec_id"], item["index"], item["spec_name"]);
                items2.push(expertise_speciality);
            }
            return [items2, ex_type];
        });
    }

    select_last_request(values)
    {
        let select_last_request = fs.readFileSync('./sql/select_last_request.sql').toString();

        let items = this.storage.readItems(select_last_request, values);
        return items.then(items => {
            let item = items[0];
            if(item == null) return item;

            const authority = new Authority(item["auth_id"], item["auth_name"]);
            const commission = new Commission(item["commission"], authority);
            const commission_decision_type = new CommissionDecisionType(item["com_dec_id"], item["com_dec_type"]);
            const commission_decision = new CommissionDecision(item["commission_decision_number"], item["commission_decision_date"], commission_decision_type);
            return [commission, commission_decision];
        });
    }

    async getExpertCertificates(expertId) 
    {
        let select_certificates = fs.readFileSync('./sql/select_certificates.sql').toString();
        const values = {id: expertId};

        let certificates = await this.storage.readItems(select_certificates, values);
        
        let certificates2 = [];
        for(const item of certificates) {

            const values2 = {id: item["id"]};

            let spec_ex_type = await this.select_cer_spec_types(values2);
            let com_com_dec = await this.select_last_request(values2);
          
            const certificate = new Certificate(item["id"], item["document_number"], item["validity_end_date"], spec_ex_type[0], spec_ex_type[1], com_com_dec[0], com_com_dec[1]);
            certificates2.push(certificate);
        }
        return certificates2;
    }



    async getClasses() 
    {
        let select_classes = fs.readFileSync('./sql/select_classes.sql').toString();
        let select_ex_types = fs.readFileSync('./sql/select_ex_types.sql').toString();
        let select_ex_spec_types = fs.readFileSync('./sql/select_ex_spec_types.sql').toString();
        
        let classes = await this.storage.readItems(select_classes, {});
        let classes2 = [];
        for(const item of classes) {
            const values = {class_id: item["id"]};
            let types = await this.storage.readItems(select_ex_types, values);
            let types2 = [];
            for(const item of types) {
                const values2 = {expertise_id: item["id"]};
                let specs = await this.storage.readItems(select_ex_spec_types, values2);
                let specs2 = [];
                for(const item of specs) {
                    const spec = new ExpertiseSpeciality(item["id"], item["index"], item["name"]);
                    specs2.push(spec);
                }
                const type = new ExpertiseType(item["id"], item["expertise_number"], item["name"], specs2);
                types2.push(type);
            }
            const ex_class = new ExpertiseClass(item["id"], item["class_number"], item["name"], types2);
            classes2.push(ex_class);
        }
        return classes2;
    }

    getAuthorities() 
    {
        let select_authorities = fs.readFileSync('./sql/select_authorities.sql').toString();
        
        let items = this.storage.readItems(select_authorities, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const authority = new Authority(item["id"], item["name"]);
                items2.push(authority);
            }
            return items2;
        });
    }

    getComDecTypes() 
    {
        let select_com_dec_types = fs.readFileSync('./sql/select_com_dec_types.sql').toString();
        
        let items = this.storage.readItems(select_com_dec_types, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const type = new CommissionDecisionType(item["id"], item["type"]);
                items2.push(type);
            }
            return items2;
        });
    }

    getRegions() 
    {
        let select_regions = fs.readFileSync('./sql/select_regions.sql').toString();
        
        let items = this.storage.readItems(select_regions, {});
        return items.then(items => {
            let items2 = [];
            for(const item of items) {
                const region = new Region(item["id"], item["name"]);
                items2.push(region);
            }
            return items2;
        });
    }
};
 
module.exports = RegisterAccess;

