var express = require('express');
var router = express.Router();
const expertiseController = require('../controllers/expertises');


router.get('/classes', expertiseController.getClasses);

router.get('/authorities', expertiseController.getAuthorities);

router.get('/commissions/decisions/types', expertiseController.getComDecTypes);

module.exports = router;