var express = require('express');
var router = express.Router();
const requestController = require('../controllers/requests');


router.get('/', requestController.getRequests);

router.get('/totalCount', requestController.getRequestTotalCount);

router.get('/statuses', requestController.getStatuses);

router.get('/operationTypes', requestController.getOperationTypes);

router.get('/:id', requestController.getRequestById);

router.patch('/:id', requestController.updateRequest);

router.post('/', requestController.addRequest);

module.exports = router;