var express = require('express');
var router = express.Router();
const expertController = require('../controllers/experts');


router.get('/', expertController.getExperts);

router.get('/totalCount', expertController.getExpertTotalCount);

router.get('/:id', expertController.getExpertById);

router.get('/:id/certificates', expertController.getExpertCertificates);

module.exports = router;