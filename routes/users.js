var express = require('express');
var router = express.Router();
const userController = require('../controllers/users');


router.get('/', userController.getUsers);

router.get('/totalCount', userController.getUserTotalCount);

router.get('/roles', userController.getUserRoles);

router.get('/:id', userController.getUserById);

router.patch('/:id', userController.updateUser);

module.exports = router;