var express = require('express');
var router = express.Router();
const logController = require('../controllers/logs');


router.get('/', logController.getLogs);

router.get('/totalCount', logController.getLogTotalCount);

router.get('/typesOfChanges', logController.getTypesOfChanges);

module.exports = router;