var express = require('express');
var router = express.Router();
const regionController = require('../controllers/regions');


router.get('/', regionController.getRegions);

module.exports = router;