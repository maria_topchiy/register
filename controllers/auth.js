const AccountAccess = require('../repositories/AccountAccess');
const authrep = new AccountAccess();
const Error = require('../models/error');
const jwt = require('jsonwebtoken');
const LogAccess = require('../repositories/LogAccess');
const logrep = new LogAccess();

function check_token(authorization) 
{
    let token;
    if(authorization == undefined) {
        const error = new Error("you are not logged in");
        return [false, error];
    } 
    else {
        try {
            token = jwt.verify(authorization.split(' ')[1], 'tynyjhrdge');
        } catch (err) {
            const error = new Error(err.message);
            return [false, error];
        }
    }
    if(token['role_id'] != 1) {
        const error = new Error("not enough permission");
        return [false, error];
    }
    return [true, token];
}

module.exports = 
{
    register(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        reg = authrep.register(req.body);
        reg.then(reg => {
            if(reg == null) {
                const error = new Error("Bad request");
                return res.status(400).send(error);
            }
            logrep.addLog(c[1]['id'], 1, reg['id'], null);
            return res.send(reg);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    login(req, res) 
    {
        log = authrep.login(req.body);
        log.then(log => {
            if(log == null) {
                const error = new Error("Incorrect login or password");
                return res.status(400).send(error);
            } 
            return res.status(200).send(log);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    }
};
