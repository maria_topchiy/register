const RequestAccess = require('../repositories/RequestAccess');
const requestrep = new RequestAccess();
const Error = require('../models/error');
const jwt = require('jsonwebtoken');
const LogAccess = require('../repositories/LogAccess');
const logrep = new LogAccess();

function check_token(authorization) 
{
    let token;
    if(authorization == undefined) {
        const error = new Error("you are not logged in");
        return [false, error];
    } 
    else {
        try {
            token = jwt.verify(authorization.split(' ')[1], 'tynyjhrdge') ;
        } catch (err) {
            const error = new Error(err.message);
            return [false, error];
        }
    }
    return [true, token];
}

module.exports = 
{
    getRequests(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        requests = requestrep.getRequests(req.query['offset'], req.query['limit'], 
            req.query['authorId'], req.query['statusId']);
        requests.then(requests => {
            return res.send(requests);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getRequestById(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        request = requestrep.getRequestById(req.params['id']);
        request.then(request => {
            if(request == null) {
                const error = new Error("no request with this ID found");
                return res.status(404).send(error);
            } 
            return res.send(request);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    updateRequest(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        request = requestrep.updateRequest(req.params['id'], req.body);
        request.then(request => {
            if(request == null) {
                const error = new Error("no request with this ID found");
                return res.status(404).send(error);
            }
            if(request.status['id'] == 2) {
                logrep.addLog(c[1]['id'], 4, request.author['id'], request['id']);
            }
            else if(request.status['id'] == 3) {
                logrep.addLog(c[1]['id'], 5, request.author['id'], request['id']);
            }
            return res.send(request);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    addRequest(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        request = requestrep.addRequest(req.body, c[1]);
        request.then(request => {
            if(request == null) {
                const error = new Error("Bad request");
                return res.status(400).send(error);
            } 
            logrep.addLog(c[1]['id'], 6, c[1]['id'], request['id']);
            return res.send(request);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getRequestTotalCount(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        count = requestrep.getRequestTotalCount(req.query['authorId'], req.query['statusId']);
        count.then(count => {
            return res.send(count);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getStatuses(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        statuses = requestrep.getStatuses();
        statuses.then(statuses => {
            return res.send(statuses);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getOperationTypes(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        types = requestrep.getOperationTypes();
        types.then(types => {
            return res.send(types);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },
};
