const AccountAccess = require('../repositories/AccountAccess');
const userrep = new AccountAccess();
const Error = require('../models/error');
const jwt = require('jsonwebtoken');
const LogAccess = require('../repositories/LogAccess');
const auth = require('./auth');
const logrep = new LogAccess();

function check_token(authorization) 
{
    let token;
    if(authorization == undefined) {
        const error = new Error("you are not logged in");
        return [false, error];
    } 
    else {
        try {
            token  = jwt.verify(authorization.split(' ')[1], 'tynyjhrdge');
        } catch (e) {
            return [false, e];
        }
    }
    // if(token['role_id'] != 1) {
    //     const error = new Error("not enough permission");
    //     return [false, error];
    // }
    return [true, token];
}

module.exports = 
{
    getUsers(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }
        
        users = userrep.getUsers(req.query['offset'], req.query['limit'], 
            req.query['firstName'], req.query['lastName'], req.query['patronymic'], 
            req.query['roleId']);
        users.then(users => {
            return res.send(users);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getUserById(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        user = userrep.getUserById(req.params['id']);
        user.then(user => {
            if(user == null) {
                const error = new Error("no user with this ID found")
                return res.status(404).send(error);
            } 
            return res.send(user);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getUserTotalCount(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        count = userrep.getUserTotalCount(req.query['firstName'], req.query['lastName'], 
            req.query['patronymic'],req.query['roleId']);
        count.then(count => {
            return res.send(count);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getUserRoles(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        roles = userrep.getUserRoles();
        roles.then(roles => {
            return res.send(roles);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    updateUser(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        user = userrep.updateUser(req.params['id'], req.body);
        user.then(user => {
            if(user == null) {
                const error = new Error("no user with this ID found")
                return res.status(404).send(error);
            } 
            
            if(!user['isActive']) {
                logrep.addLog(c[1]['id'], 2, user['id'], null);
            }
            logrep.addLog(c[1]['id'], 3, user['id'], null);

            return user;
        })
        .then((user) => res.send(user))
        .catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    }
};
