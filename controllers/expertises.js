const RegisterAccess = require('../repositories/RegisterAccess');
const expertiserep = new RegisterAccess();
const Error = require('../models/error');

module.exports = 
{
    getClasses(req, res) 
    {
        classes = expertiserep.getClasses();
        classes.then(classes => {
            return res.send(classes);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getAuthorities(req, res) 
    {
        authorities = expertiserep.getAuthorities();
        authorities.then(authorities => {
            return res.send(authorities);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getComDecTypes(req, res) 
    {
        types = expertiserep.getComDecTypes();
        types.then(types => {
            return res.send(types);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    }
};
