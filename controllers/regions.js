const RegisterAccess = require('../repositories/RegisterAccess');
const regionrep = new RegisterAccess();
const Error = require('../models/error');

module.exports = 
{
    getRegions(req, res) 
    {
        regions = regionrep.getRegions();
        regions.then(regions => {
            return res.send(regions);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    }
};
