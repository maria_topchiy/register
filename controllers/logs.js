const LogAccess = require('../repositories/LogAccess');
const logrep = new LogAccess();
const Error = require('../models/error');
const jwt = require('jsonwebtoken');

function check_token(authorization) 
{
    let token;
    if(authorization == undefined) {
        const error = new Error("you are not logged in");
        return [false, error];
    } 
    else {
        try {
            token = jwt.verify(authorization.split(' ')[1], 'tynyjhrdge') 
        } catch (err) {
            const error = new Error(err.message);
            return [false, error];
        }
    }
    if(token['role_id'] != 1) {
        const error = new Error("not enough permission");
        return [false, error];
    }
    return [true, token];
}

module.exports = 
{
    getLogs(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        logs = logrep.getLogs(req.query);
        logs.then(logs => {
            return res.send(logs);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getLogTotalCount(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        count = logrep.getLogTotalCount(req.query);
        count.then(count => {
            return res.send(count);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getTypesOfChanges(req, res) 
    {
        let c = check_token(req.headers.authorization);
        if(!c[0]) {
            return res.status(400).send(c[1]);
        }

        types = logrep.getTypesOfChanges();
        types.then(types => {
            return res.send(types);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    }
};
