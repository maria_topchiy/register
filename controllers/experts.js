const RegisterAccess = require('../repositories/RegisterAccess');
const expertrep = new RegisterAccess();
const Error = require('../models/error');

module.exports = 
{
    getExperts(req, res) 
    {
        experts = expertrep.getExperts(req.query['offset'], req.query['limit'], 
            req.query['firstName'], req.query['lastName'], req.query['patronymic'], 
            req.query['regionId'], req.query['isFromGovernment'], req.query['authorityId'], 
            req.query['expertiseSpecialityId'], req.query['expertiseTypeId'], req.query['expertiseClassId']);
        experts.then(experts => {
            return res.send(experts);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getExpertById(req, res) 
    {
        expert = expertrep.getExpertById(req.params['id']);
        expert.then(expert => {
            if(expert == null) {
                const error = new Error("no expert with this ID found");
                return res.status(404).send(error);
            } 
            return res.send(expert);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getExpertTotalCount(req, res) 
    {
        count = expertrep.getExpertTotalCount(req.query['firstName'], req.query['lastName'], 
            req.query['patronymic'],req.query['regionId'], req.query['isFromGovernment'], 
            req.query['authorityId'], req.query['expertiseSpecialityId'], 
            req.query['expertiseTypeId'], req.query['expertiseClassId']);
        count.then(count => {
            return res.send(count);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },

    getExpertCertificates(req, res) 
    {
        certificates = expertrep.getExpertCertificates(req.params['id']);
        certificates.then(certificates => {
            return res.send(certificates);
        }).catch(function(e) {
            const error = new Error(e.message);
            return res.status(500).send(error);
        });
    },
};
