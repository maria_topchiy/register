require('dotenv').config();

const config = {
  app: {
    port: parseInt(process.env.PORT),
    pub_port: parseInt(process.env.PUB_PORT)
  },
  db: {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    name: process.env.DB_NAME,
    user: process.env.USER_NAME,
    password: process.env.USER_PASSWORD
  }
};
   
module.exports = config;