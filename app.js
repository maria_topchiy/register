var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var expertsRouter = require('./routes/experts');
var usersRouter = require('./routes/users');
var requestsRouter = require('./routes/requests');
var logsRouter = require('./routes/logs');
var expertisesRouter = require('./routes/expertises');
var authRouter = require('./routes/auth');
var regionsRouter = require('./routes/regions');
var cors = require('cors');

var app = express();

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./register-openapi.yaml');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const bodyParser = require('body-parser');

app.use(cors());  // Enable All CORS Requests

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.raw());

app.use('/', indexRouter);
app.use('/api/experts', expertsRouter);
app.use('/api/users', usersRouter);
app.use('/api/requests', requestsRouter);
app.use('/api/logs', logsRouter);
app.use('/api/expertise', expertisesRouter);
app.use('/api/auth', authRouter);
app.use('/api/regions', regionsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
